'use strict';
// --------------------------------------------- //
// ------- Random Game Kristofer Brink --------- //
// -------- Created by Nikhil Suresh ----------- //
// -------- Three.JS is by Mr. doob  ----------- //
// --------------------------------------------- //


// ------------------------------------- //
// ------- GLOBAL VARIABLES ------------ //
// ------------------------------------- //

// scene object variables
var renderer, scene, camera;
var playerBall;
var cameraPos = new THREE.Vector3(0,0,0);
var playerBallRadius = 5;
var chancePerFrame = 10;
var colladaLoader = new THREE.ColladaLoader();
var body;

var extendTimer = 0;
var score = 0;
var scoreElem;
var unitLength = 20;
var tableLengthUnits = 30
var tableSide = unitLength * tableLengthUnits;

jQuery(document).ready(function () {
    scoreElem = jQuery('#score');
    body = jQuery('body');
    setup();
});


var myLoadImage, myLoadTexture;
(function (){
    var imageLoader = new THREE.ImageLoader();
    myLoadImage = function () {
        arguments[0] = arguments[0];
        return imageLoader.load.apply(imageLoader, arguments);
    };
    
    var textureLoader = new THREE.TextureLoader();
    textureLoader.crossOrigin = 'anonymous';
    myLoadTexture = function () {
        arguments[0] = arguments[0];
        return textureLoader.load.apply(textureLoader, arguments);
    };
})();

    


function setup()
{
    scoreElem.text('0');
    // Set up physics
	Physijs.scripts.worker = 'Scripts/physijs_worker.js';
    Physijs.scripts.ammo = 'ammo.js';

    // set up all the 3D objects in the scene	
	createScene();
	
   // and let's get cracking!
   draw();
}

function setSceneBasics(){
    // set the scene size
	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer({antialias: true, logarithmicDepthBuffer: true});
    renderer.shadowMap.enabled = true;
    
    // Make camera
	camera =
	  new THREE.PerspectiveCamera(
		63, // View angle 
		WIDTH / HEIGHT, // Aspect
		0.1, // Near
		3000000); // Far
    // rotate to face towards the opponent
    
    camera.rotation.x = -30 * Math.PI/180;
	camera.rotation.y = -30 * Math.PI/180;
    camera.position.set(0, 512.5, 150);
    camera.lookAt(new THREE.Vector3(0, -120, 0));


    
    scene = new Physijs.Scene();
    var winResize   = new THREEx.WindowResize(renderer, camera)
	
    // add the camera to the scene
	scene.add(camera);
	
	// set a default position for the camera
	// not doing this somehow messes up shadow rendering well this doesn't do anything says Kristofer Brink
	//camera.position.z = 200;
	
	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);
}

function setGravity() {
    scene.setGravity(new THREE.Vector3( 0, -200, 0 ));
}

function makeTable(){
    // --------------------------------------------------------------------------
	// Make the box the game is going to take place in.
    var gameBoxMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ map: myLoadTexture( 'images/textures/sand.png' )}),
        .8, // high friction
        0 // low restitution
    );
    
    
    gameBoxMaterial.map.wrapS = gameBoxMaterial.map.wrapT = THREE.RepeatWrapping;
    gameBoxMaterial.map.repeat.set( 1, 1);
    
    // ------------------------------------------- ^
        var wall = new Physijs.BoxMesh(
		  new THREE.BoxGeometry( 
                tableSide,
                unitLength,
                tableSide),
            gameBoxMaterial,
            0);
        wall.position.set(0, -playerBallRadius/2, 0);
        //console.log('Make wall with rotation=' + wallInfo.r + ' and position=' + wall.position.toArray().join(','));

		wall.castShadow = true;
		wall.receiveShadow = true;
		scene.add(wall);
}





var light;
var snake;
var food;
function createScene()
{

    setSceneBasics();
    
    setGravity();

    makeTable(); 

    // MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
    renderer.shadowMap.enabled = true;
    //renderer.shadowMapType = THREE.PCFSoftShadowMap; // snooth shadows

    /* Call the playerBallMovement() once every physics (simulation) frame. */
    snake = new Snake(new THREE.Vector2(0, 0), new THREE.Vector2(0, -1), Key.arrowUp, Key.arrowDown, Key.arrowLeft, Key.arrowRight, '#349967', unitLength, 'Sam', .1, false);
    
    var wallSpeed = .5;
    var snakeWallRight = new Snake(new THREE.Vector2(tableSide/2, tableSide/2), new THREE.Vector2(0, -1), Key.aha, Key.aha, Key.aha, Key.aha, '#123456', unitLength, 'Wall Right', wallSpeed, true);
    var snakeWallLeft = new Snake(new THREE.Vector2(-tableSide/2, -tableSide/2), new THREE.Vector2(0, 1), Key.aha, Key.aha, Key.aha, Key.aha, '#123456', unitLength, 'Wall Left', wallSpeed, true);
    var snakeWallTop = new Snake(new THREE.Vector2(-tableSide/2, tableSide/2), new THREE.Vector2(1, 0), Key.aha, Key.aha, Key.aha, Key.aha, '#123456', unitLength, 'Wall Top', wallSpeed, true);
    var snakeWallBottom = new Snake(new THREE.Vector2(tableSide/2, -tableSide/2), new THREE.Vector2(-1, 0), Key.aha, Key.aha, Key.aha, Key.aha, '#123456', unitLength, 'Wall Bottom', wallSpeed, true);
    
    food = new Food(unitLength, '#845935')
    
    
    scene.addEventListener('update', animate);
    scene.addEventListener('update', function () {snake.animate(); snakeWallRight.animate(); snakeWallLeft.animate(); snakeWallTop.animate(); snakeWallBottom.animate(); food.animate();});
    makePointLight();
}



function makePointLight(){
    jQuery.map([
        {yOffset: 150, xOffset: function(){ return snake.headSegment.getHeadPosition().x;}, zOffset: function(){return snake.headSegment.getHeadPosition().y;}
         , color: 0xffffff, distance: 1000, decay: .1, intensity: 2},
        
  ], function (lightInfo) {
        var light = new THREE.PointLight(
            lightInfo.color, 
            lightInfo.intensity || 1, // Intensity
            lightInfo.distance, // Distance
            lightInfo.decay); // decay
            light.castShadow = true;
            light.shadowCameraNear = 1;
            light.shadowCameraFar = tableSide;
            light.shadowDarkness = 0.5;
            light.shadowMapWidth = 2048;
            light.shadowMapHeight = 2048;
        
      var setLightPosition = function () {
          light.position.set(lightInfo.xOffset(), 
                             lightInfo.yOffset, 
                             lightInfo.zOffset());
      };
      setLightPosition();
      scene.addEventListener('update', setLightPosition);
      scene.add(light);
  });
}



function draw()
{
	// Physijs simulate
    if(!Key.isDown(Key.P)) {
	   scene.simulate(); // run physics
    }
	// draw THREE.JS scene

	renderer.render(scene, camera);
    
    // loop draw function call
	requestAnimationFrame(draw);
}



// Handles player's movement
function animate()
{
    // Keep player ball in the same place
    // --------------------- P  layer movement ------
    if (snake.alive == false) {
         body.addClass('gameover');
        if (Key.isDown(Key[' '])) {
            
        }
    }
    
}

function Food(unitLength, color){
    this.snake = snake;
    // One food is always there and is random.
    var material = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: color}),
        .0, // high friction
        .0 // low restitution
    );
    this.unitLength = unitLength;
    this.box = new Physijs.BoxMesh(
        new THREE.BoxGeometry( 
        this.unitLength, this.unitLength, this.unitLength),
        material,
        0); 
    this.box.receiveShadow = true;
    this.box.castShadow = true;
    this.box.userData = { food: true };
    scene.add(this.box);
    this.moveFood();
}
jQuery.extend(
    Food.prototype,{
        animate: function(){
            
        },
        moveFood: function(){
            this.box.position.set(this.randomPlace(), this.unitLength/2, this.randomPlace());
            this.box.__dirtyPosition = true;
        },
        randomPlace: function(){
            return tableLengthUnits * unitLength/2 - Math.floor(Math.random() * (tableLengthUnits - 2) + 1) * unitLength;
        }
    });

// Snake --------------------------------------------------------------------
  
function Snake(position, direction, up, down, left, right, color, unitLength, name, dexetendIncrement, extending)
{
    this.snakeLength = 1;
    this.extending = extending;
    this.alive = true;
    var snakeMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: color}),
        .0, // high friction
        .0 // low restitution
    );
    this.up = up;
    this.down = down;
    this.left = left;
    this.right = right;
    this.headSegment = new SnakeSegment(
        this,
        position,
        direction, undefined, snakeMaterial, unitLength, dexetendIncrement);
    
    this.snakeHead = new SnakeHead(unitLength, position, this);
    
    this.tailSegment = this.headSegment;
    this.turnDirection = direction.clone();
    this.name = name;
}

jQuery.extend(
    Snake.prototype,
    {
        animate: function() {
            if (!this.alive){
                return;
            }
            this.headSegment.extend();
            if (!this.extending) {
                if (this.tailSegment.shrink()) {
                    this.tailSegment.destroy();
                    this.tailSegment = this.tailSegment.next;
                }
            }
            this.getTurn();
           
            var headSegmentLengthModulus = this.headSegment.length % 1;
            if (headSegmentLengthModulus < this.lastHeadSegmentLengthModulus)
               this.turn();
            this.lastHeadSegmentLengthModulus = headSegmentLengthModulus;

            this.snakeHead.animate(this.headSegment.getHeadPosition());
        },
        getTurn: function () {
            if (this.headSegment.direction.x) {
                if (Key.isDown(this.up)){
                    this.turnDirection = new THREE.Vector2(0,-1);
                }
                if (Key.isDown(this.down)) {
                    this.turnDirection = new THREE.Vector2(0,1);
                }
            } else {
                if (Key.isDown(this.left)) {
                    this.turnDirection = new THREE.Vector2(-1,0);
                }
                if (Key.isDown(this.right)) {
                    this.turnDirection = new THREE.Vector2(1,0);
                }
            }
        },
        turn: function () {
            if (!this.headSegment.direction.equals(this.turnDirection)) {
                this.headSegment = this.headSegment.turn(this.turnDirection);
            }
        },
        opacity: function(opacity){
            
        }
    });

function SnakeSegment(snake, position, direction, previous, material, unitLength, dexetendIncrement) {
    this.dextendIncrement = dexetendIncrement;
    this.startPosition = position;
    this.direction = direction;
    this.length = 1;
    this.advancement = 0;
    this.unitLength = unitLength;
    
    this.box = new Physijs.BoxMesh(
        new THREE.BoxGeometry( 
        this.unitLength, this.unitLength, this.unitLength),
        material,
        0); // Gravity;
    
    this.box.receiveShadow = true;
    this.box.castShadow = true;
    this.box.position.set(position.x, unitLength / 2, position.y);
    this.box.userData = { snake: snake };
    scene.add(this.box);
    
    this.previous = previous;
    if (this.previous) {
        this.previous.next = this;
    }
}

jQuery.extend(
    SnakeSegment.prototype,
    {
        destroy: function () {
            scene.remove(this.box);
            if (this.next) {
                this.next.previous = undefined;
            }
        },
        getHeadPosition: function () {
            return this.startPosition.clone().add(this.direction.clone().multiplyScalar(this.unitLength * (this.length-1)));
        },
        extend: function (amount) {
            amount = amount === undefined ? this.dextendIncrement : amount;
            this.length += amount;
            this.updateBox();
        },
        shrink: function (amount) {
            var overShrink;
            amount = amount === undefined ? this.dextendIncrement : amount;
            /*
             * We have to use advancement when we are the head.
             * But if we use advancement when we are not the head,
             * the calculations which assume we are the head fail
             * and make the thing move when it shouldn't. Thus we
             * cheat by switching the startPosition around and
             * decreasing length.
             */
            if (this.next) {
                this.length -= amount;
            } else {
                this.advancement += amount;
            }
            this.updateBox();
            
            /* Detect becoming so short that I need to disappear and start shrinking the next segment. */
            overShrink = 1 - (this.length - this.advancement);
            if (overShrink >= 0 && this.next) {
                this.next.shrink(overShrink);
                return true;
            }
        },
        roundLength: function () {
            this.length = Math.round(this.length);
            this.updateBox();
        },
        updateBox: function () {
            var position, scale;
            position = this.startPosition.clone().add(this.direction.clone().multiplyScalar(this.unitLength*((this.length-1 + this.advancement)/2)));
            scale = this.direction.clone().multiplyScalar(this.length - this.advancement);
            this.box.position.set(position.x, this.box.position.y, position.y);
            this.box.__dirtyPosition = true;
            this.box.scale.set(Math.abs(scale.x || 1), this.box.scale.y, Math.abs(scale.y || 1));
        },
        turn: function (direction) {
            var originalLength = this.length;
            this.roundLength();
            this.next = new SnakeSegment(
                this.box.userData.snake,
                this.getHeadPosition(),
                direction,
                this, this.box.material, this.unitLength, this.dextendIncrement);
            
            /* Because we called roundLength() to round down the length, need to make up or that by pre-extending the new segment. */
            this.next.extend(originalLength - this.length);
            
            /* Transpose around the startPosition so that something can happen. */
            this.startPosition = this.getHeadPosition();
            this.direction.multiplyScalar(-1);
            this.length -= this.advancement;
            this.advancement = 0;
            
            /* Return the new segment to caller. */
            return this.next;
        }
    });

function SnakeHead(unitLength, position, snake) {
    this.raycaster = new THREE.Raycaster(new THREE.Vector3(position.x, unitLength/2, position.y), new THREE.Vector3(0,1,0), 0, unitLength/2.1);
    this.snake = snake;
    this.timeForOneExtend = 1/snake.headSegment.dextendIncrement;
    if (snake.extending) {
        this.extendTimer = -this.timeForOneExtend;
    } else {
        this.extendTimer = 0;
    }
}

jQuery.extend(
    SnakeHead.prototype,
    {
        animate: function (position) {
            var headPosition = this.snake.headSegment.getHeadPosition();
            var headDirection = this.snake.headSegment.direction;
            this.raycaster.set(
                new THREE.Vector3(headPosition.x, this.snake.headSegment.unitLength/2, headPosition.y),
                new THREE.Vector3(headDirection.x, 0, headDirection.y));
            var other = this.raycaster.intersectObjects(scene.children)[0];
            if (other) {
                this.hit(other.object);
            }
            if (this.extendTimer == -this.timeForOneExtend){
                return;
            }
            if (this.extendTimer>0){
                this.extendTimer-=1;
            } else {
                this.snake.extending = false;
            }
        },  
        hit: function (other) {
            var otherSnake;
            
            otherSnake = (other.userData || {}).snake;
            if (otherSnake) {
                this.snake.alive = false;
            }
            
            var foodBool = other.userData.food
            
            if (foodBool) {
                this.snake.headSegment.dextendIncrement += .0025;
                this.timeForOneExtend = 1/this.snake.headSegment.dextendIncrement;
                this.extendTimer = this.timeForOneExtend;
                this.snake.extending = true;
                score += 1;
                if (scoreElem)
                    scoreElem.text(String(score));
                food.moveFood();
            }
        }
    });
