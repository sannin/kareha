'use strict';
// --------------------------------------------- //
// ------- Random Game Kristofer Brink --------- //
// -------- Created by Nikhil Suresh ----------- //
// -------- Three.JS is by Mr. doob  ----------- //
// --------------------------------------------- //


// ------------------------------------- //
// ------- GLOBAL VARIABLES ------------ //
// ------------------------------------- //

// scene object variables
var renderer, scene, camera;
var playerBall;
var cameraPos = new THREE.Vector3(0,0,0);
var avalanche = [];
var playerBallRadius = 3;
var wallHeight = 1000;
var tableWidth = 50*playerBallRadius;
var chancePerFrame = 10;
var colladaLoader = new THREE.ColladaLoader();
var body;
var keyBool = false;
var pipeCounter = 0;

var score = 0;
var scoreElem;

jQuery(document).ready(function () {
    scoreElem = jQuery('#score');
    body = jQuery('body');
});

function deltaScore(x) {
    score += x;
    if (scoreElem)
        scoreElem.text(String(score));
}

var myLoadImage, myLoadTexture;
(function (){
    var imageLoader = new THREE.ImageLoader();
    myLoadImage = function () {
        arguments[0] = arguments[0];
        return imageLoader.load.apply(imageLoader, arguments);
    };
    
    var textureLoader = new THREE.TextureLoader();
    textureLoader.crossOrigin = 'anonymous';
    myLoadTexture = function () {
        arguments[0] = arguments[0];
        return textureLoader.load.apply(textureLoader, arguments);
    };
})();


function setup()
{
    
	
    // Set up physics
	Physijs.scripts.worker = 'Scripts/physijs_worker.js';
    Physijs.scripts.ammo = 'ammo.js';

    // set up all the 3D objects in the scene	
	createScene();
	
   // and let's get cracking!
   draw();
}

function setSceneBasics(){
    // set the scene size
	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer({antialias: true, logarithmicDepthBuffer: true});
    renderer.shadowMap.enabled = true;
    
    // Make camera
	camera =
	  new THREE.PerspectiveCamera(
		15, // View angle
		WIDTH / HEIGHT, // Aspect
		0.1, // Near
		3000000); // Far
    // rotate to face towards the opponent
    camera.rotation.x = -30 * Math.PI/180;
	camera.rotation.y = -30 * Math.PI/180;
    new THREEx.WindowResize(renderer, camera);
    
    scene = new Physijs.Scene();
	
    // add the camera to the scene
	scene.add(camera);
	
	// set a default position for the camera
	// not doing this somehow messes up shadow rendering well this doesn't do anything says Kristofer Brink
	//camera.position.z = 200;
	
	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);
}

function setGravity() {
    scene.setGravity(new THREE.Vector3( 0, -200, 0 ));
}

function makePlayerBox(){
     // ------------------------------------------- playerBall
	// // create the playerBall's material
    
    // Materials
    
    var playerBallMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: '#abcdef', transparent: true, opacity: .9 }),
        .8, // high friction
        .0 // low restitution
    );


    playerBall = new Physijs.BoxMesh(
       new THREE.SphereGeometry(
          playerBallRadius, 
          100,
          100), 
        playerBallMaterial,
        1); // Gravity

    playerBall.receiveShadow = true;
    playerBall.castShadow = true;
    playerBall.position.set(0,50,0);

    // // add the sphere to the scene
    scene.add(playerBall);
    // Slow down the ball
    playerBall.setDamping( .1, 0);

    playerBall.setAngularFactor(new THREE.Vector3(2,2,2));

    playerBall.rotation.set(new THREE.Vector3(0, 25, 90));

    var redMaterial = new THREE.MeshLambertMaterial({color: 0xff0000});
    var greenMaterial = new THREE.MeshLambertMaterial({color: 0x44ff44});
    playerBall.addEventListener('collision', function (other) {
        var otherUserData = other.userData || {};
        if (otherUserData.badguy) {
            other.material = redMaterial;
            //deltaScore(-1000);
        }
        if (otherUserData.goodguy) {
            other.material = greenMaterial;
            deltaScore(9000);
        }
    });
}


var gameBoxMaterial;

function makeTable(){
    // --------------------------------------------------------------------------
	// Make the box the game is going to take place in.
    gameBoxMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ map: myLoadTexture( 'images/textures/wood.jpg' )}),
        .8, // high friction
        0 // low restitution
    );
    
    
    gameBoxMaterial.map.wrapS = gameBoxMaterial.map.wrapT = THREE.RepeatWrapping;
    gameBoxMaterial.map.repeat.set( 2, 2);
    
    // ------------------------------------------- ^
        var wall = new Physijs.BoxMesh(
		  new THREE.BoxGeometry( 
                tableWidth,
                playerBallRadius,
                tableWidth/2),
            gameBoxMaterial,
            0);
        wall.position.set(0, -playerBallRadius/2, 0);
        //console.log('Make wall with rotation=' + wallInfo.r + ' and position=' + wall.position.toArray().join(','));

		wall.castShadow = true;
		wall.receiveShadow = true;
		scene.add(wall);
}


var pointLightYOffset = -70000;
function makePointLight(){
    jQuery.map([
        /*
        {xOffset: 0, yOffset: pointLightYOffset, zOffset: 300, color: 0xff0000, distanceOffset: 0},
        {xOffset: 200 * Math.cos(Math.PI * -1/6), yOffset: pointLightYOffset, zOffset: 300 * Math.sin(Math.PI * -1/6), color: 0x00ff00, distanceOffset: 0},
        {xOffset: 200 * Math.cos(Math.PI * -5/6), yOffset: pointLightYOffset, zOffset: 300 * Math.sin(Math.PI * -1/6), color: 0x0000ff, distanceOffset: 0},*/
        //{yOffset: -wallHeight / 10, zOffset: 0, color: 0xffffff, distance: wallHeight/ 8, decay: 2, posChange: false},
        //{yOffset: wallHeight/ 1.5, color: 0x99ffff, distance: wallHeight / 2, decay: 1, intensity: 0.5},
        //{yOffset: wallHeight / 4, color: 0xffff77, distance: wallHeight, decay: 0},
        {yOffset: 40, color: 0xffffff, distance: 10, decay: 0, intensity: 2},
        //{yOffset: wallHeight / 16, color: 0xffffff, distance: wallHeight / 4 , decay: 0, intensity: .3},
        //{yOffset: -wallHeight / 5, color: 0xffffff, distance: wallHeight , decay: 0, intensity: .5}
  ], function (lightInfo) {
        var light = new THREE.PointLight(
            lightInfo.color, 
            lightInfo.intensity || 1, // Intensity
            lightInfo.distance, // Distance
            lightInfo.decay); // decay
         light.castShadow = true;
        light.shadowCameraNear = 1;
        light.shadowCameraFar = wallHeight * 4;
        light.shadowDarkness = 0.5;
        light.shadowMapWidth = 2048;
        light.shadowMapHeight = 2048;

      var setLightPosition = function () {
          light.position.set(-10, 
                             lightInfo.yOffset, 
                             0);
      };
      setLightPosition();
      scene.addEventListener('update', setLightPosition);
      scene.add(light);
  });
     }

/*
funciton makeTextMesh(){
    var i = 1;
}

function makeText3D(){
    var textMaterial = new THREE.MeshPhongMaterial({
        color: 0xdddddd
    });
    var textGeom = new THREE.TextGeometry( 'Hello World!', {
        font: 'harabara' // Must be lowercase!
        , size: 10
        , height: 10
    });
    var textMesh = new THREE.Mesh( textGeom, textMaterial );

    scene.add( textMesh );
}
*/

function makeSkyBox(){
    var cubeMap = new THREE.CubeTexture( [] );
    cubeMap.format = THREE.RGBFormat;

    myLoadImage( 'images/textures/sky/skyboxsun25degtest.png', function ( image ) {
        if (!image)
            throw new Error('Unable to load something.');
        console.log(image);

        var getSide = function ( x, y ) {

            var size = 1024;

            var canvas = document.createElement( 'canvas' );
            canvas.width = size;
            canvas.height = size;

            var context = canvas.getContext( '2d' );
            context.drawImage( image, - x * size, - y * size );

            return canvas;

        };
        cubeMap.images[ 0 ] = getSide( 2, 1 ); // px
        cubeMap.images[ 1 ] = getSide( 0, 1 ); // nx
        cubeMap.images[ 2 ] = getSide( 1, 0 ); // py
        cubeMap.images[ 3 ] = getSide( 1, 2 ); // ny
        cubeMap.images[ 4 ] = getSide( 1, 1 ); // pz
        cubeMap.images[ 5 ] = getSide( 3, 1 ); // nz
        cubeMap.needsUpdate = true;
    } );

    var cubeShader = THREE.ShaderLib[ 'cube' ];
    cubeShader.uniforms[ 'tCube' ].value = cubeMap;

    var skyBoxMaterial = new THREE.ShaderMaterial( {
        fragmentShader: cubeShader.fragmentShader,
        vertexShader: cubeShader.vertexShader,
        uniforms: cubeShader.uniforms,
        depthWrite: false,
        side: THREE.BackSide
    } );

    var skyBox = new THREE.Mesh(
        new THREE.BoxGeometry( tableWidth * 4, tableWidth * 4, tableWidth * 4),
        skyBoxMaterial
    );
    

    scene.add( skyBox );
}

var light;

function createScene()
{
    
    setSceneBasics();
    
   // var light = new THREE.AmbientLight( 0x404040 ); // soft white light
    //scene.add( light );
    
    setGravity();
   
    makePlayerBox();

    makeSkyBox();

    makeTable(); 

    makePointLight();


    // MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
    renderer.shadowMap.enabled = true;
    //renderer.shadowMapType = THREE.PCFSoftShadowMap; // snooth shadows

    /* Call the playerBallMovement() once every physics (simulation) frame. */

    scene.addEventListener('update', animate);
}

function draw()
{
	// Physijs simulate
    if(!Key.isDown(Key.F)) {
	   scene.simulate(); // run physics
    }
	// draw THREE.JS scene
    
    cameraPosition();

    var i;
    for (i = 0; i < avalanche.length; i++)
        avalanche[i].onrender();

	renderer.render(scene, camera);
    
    // loop draw function call
	requestAnimationFrame(draw);
}

function createPipes(){
    pipeCounter+=1;
    if (pipeCounter == 65) {
        pipeCounter = 0;
        var randPos = Math.random() * 50;
        var i;
        for(i=0; i<2; i++){
            // Materials
            pipeLine(i, randPos);
            
        }
    }
}

function pipeLine(i, randPos) {
    var avalancheMaterial = Physijs.createMaterial(
    new THREE.MeshLambertMaterial({ color: '#adf123' }),
    10, // high friction
    .2 // low restitution
    );
    var obj;

    obj = new Physijs.BoxMesh( 
       new THREE.BoxGeometry( 
          5, 100, 5),
        avalancheMaterial,
        1); // Gravity

    obj.receiveShadow = true;
    obj.castShadow = true;

    obj.position.set(100, randPos + i * 120 - 50, 0);
    obj.userData = { badguy: true };
    scene.add(obj);
    obj.setLinearFactor(new THREE.Vector3(0, 0, 0));
    obj.setAngularFactor(new THREE.Vector3(0, 0, 0));
    obj.setLinearVelocity(new THREE.Vector3(-20, 0, 0));
    obj.setAngularVelocity(new THREE.Vector3(0, 0, 0));

    avalanche.push({
    object: obj,
    onrender: function () {},
    remove: function () {
        scene.remove(obj);
    }
    });

    obj.setAngularFactor(new THREE.Vector3(2,2,2));

    obj.rotation.set(new THREE.Vector3(0, 25, 90));
}

function animateAvalanche() {
    jQuery.each(avalanche, function () {
        this.object.rotation.set(0, 0, 0);
        this.object.__dirtyRotation = true;
    })
}

function deleteAvalanche(x) {
    var i;
    for (i = avalanche.length - 1; i >= 0; i--){
        var entry = avalanche[i];
        if (entry.object.position.x < x) {
            avalanche.splice(i, 1);
            entry.remove();
        }
    }
}



var nextScore = 1;
var animationsPerScore = 30;

// Handles player's movement
function animate()
{
    // Run this if the game is still going. Hacky way of doing this but easy and fast
    if (playerBall.position.y > -10){ 
        if (!--nextScore) {
            deltaScore(1);
            nextScore = animationsPerScore;
        }
    createPipes();
    } else {
        body.addClass('gameover');
        if(Key.isDown(Key[' '])){
            playerBall.setLinearVelocity(new THREE.Vector3(0,0,0));
            playerBall.position.set(0,50,0);
            playerBall.__dirtyPosition = true;
            deleteAvalanche(10000);
            score = 0;
            body.removeClass('gameover');
        }
    } 
    animateAvalanche();
    deleteAvalanche(-40);
    // Keep player ball in the same place
    // --------------------- Player movement ------
    
    var impulse = new THREE.Vector3(0, 0, 0);
	if (keyBool == true && (Key.isDown(Key.W) || Key.isDown(Key.arrowUp))) {
        playerBall.setLinearVelocity(new THREE.Vector3(0, 80, 0));
        keyBool = false;
    } 
    if (!(Key.isDown(Key.W) || Key.isDown(Key.arrowUp))) {
        keyBool = true;
    }
    playerBall.applyCentralImpulse(impulse);
    
}

// Handles camera and lighting logic
function cameraPosition()
{
    var camPosMax = tableWidth/2 - 1000;
    // Make the camera cooler and stuff
    var posXMax = /*(playerBall.position.x > 0)? Math.min(camPosMax, playerBall.position.x): Math.max(-camPosMax, playerBall.position.x)*/ playerBall.position.x;
    var posZMax = /*(playerBall.position.z >0)? Math.min(camPosMax, playerBall.posision.z): Math.max(-camPosMax, playerBall.position.z)*/ playerBall.position.z;
    var adfs = playerBall.getLinearVelocity();
    
    camera.position.set(
        2*playerBallRadius + playerBall.position.x + 50,
        12*playerBallRadius, 
        playerBall.position.z + 100*playerBallRadius);
    camera.lookAt(new THREE.Vector3(posXMax + 50, 40, posZMax));
}




