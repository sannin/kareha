'use strict';
// --------------------------------------------- //
// ------- Random Game Kristofer Brink --------- //
// -------- Created by Nikhil Suresh ----------- //
// -------- Three.JS is by Mr. doob  ----------- //
// --------------------------------------------- //


// ------------------------------------- //
// ------- GLOBAL VARIABLES ------------ //
// ------------------------------------- //

// scene object variables
var renderer, scene, camera;
var playerBall;
var cameraPos = new THREE.Vector3(0,0,0);
var avalanche = [];
var playerBallRadius = 5;
var wallHeight = 1000;
var tableWidth = 50*playerBallRadius;
var chancePerFrame = 10;
var colladaLoader = new THREE.ColladaLoader();
var body;

var score = 0;
var scoreElem;

jQuery(document).ready(function () {
    scoreElem = jQuery('#score');
    body = jQuery('body');
    setup();
});

function deltaScore(x) {
    score += x;
    if (scoreElem)
        scoreElem.text(String(score));
}

var myLoadImage, myLoadTexture;
(function (){
    var imageLoader = new THREE.ImageLoader();
    myLoadImage = function () {
        arguments[0] = arguments[0];
        return imageLoader.load.apply(imageLoader, arguments);
    };
    
    var textureLoader = new THREE.TextureLoader();
    textureLoader.crossOrigin = 'anonymous';
    myLoadTexture = function () {
        arguments[0] = arguments[0];
        return textureLoader.load.apply(textureLoader, arguments);
    };
})();

    


function setup()
{
    
	
    // Set up physics
	Physijs.scripts.worker = 'Scripts/physijs_worker.js';
    Physijs.scripts.ammo = 'ammo.js';

    // set up all the 3D objects in the scene	
	createScene();
	
   // and let's get cracking!
   draw();
}

function setSceneBasics(){
    // set the scene size
	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer({antialias: true, logarithmicDepthBuffer: true});
    renderer.shadowMap.enabled = true;
    
    // Make camera
	camera =
	  new THREE.PerspectiveCamera(
		50, // View angle
		WIDTH / HEIGHT, // Aspect
		0.1, // Near
		3000000); // Far
    // rotate to face towards the opponent
    
    camera.rotation.x = -30 * Math.PI/180;
	camera.rotation.y = -30 * Math.PI/180;
    camera.position.set(0, 512.5, 150);
    camera.lookAt(new THREE.Vector3(0, -120, 0));


    
    scene = new Physijs.Scene();
    var winResize   = new THREEx.WindowResize(renderer, camera)
	
    // add the camera to the scene
	scene.add(camera);
	
	// set a default position for the camera
	// not doing this somehow messes up shadow rendering well this doesn't do anything says Kristofer Brink
	//camera.position.z = 200;
	
	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);
}

function setGravity() {
    scene.setGravity(new THREE.Vector3( 0, -200, 0 ));
}

function makePlayerBox(){
     // ------------------------------------------- playerBall
	// // create the playerBall's material
    
    // Materials
    
    var playerBallMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: '#abcdef', transparent: true, opacity: .9 }),
        .8, // high friction
        .0 // low restitution
    );


    playerBall = new Physijs.BoxMesh(
       new THREE.SphereGeometry(
          playerBallRadius, 
          100,
          100), 
        playerBallMaterial,
        1); // Gravity

    playerBall.receiveShadow = true;
    playerBall.castShadow = true;
    playerBall.position.set(0,0,0);

    // // add the sphere to the scene
    scene.add(playerBall);
    // Slow down the ball
    playerBall.setDamping( .1, 0);

    playerBall.setAngularFactor(new THREE.Vector3(2,2,2));

    playerBall.rotation.set(new THREE.Vector3(0, 25, 90));

    var redMaterial = new THREE.MeshLambertMaterial({color: 0xff0000});
    var greenMaterial = new THREE.MeshLambertMaterial({color: 0x44ff44});
    playerBall.addEventListener('collision', function (other) {
        var otherUserData = other.userData || {};
        if (otherUserData.badguy) {
            other.material = redMaterial;
            //deltaScore(-1000);
        }
        if (otherUserData.goodguy) {
            other.material = greenMaterial;
            deltaScore(9000);
        }
    });
}


var tableSide = 500;

function makeTable(){
    // --------------------------------------------------------------------------
	// Make the box the game is going to take place in.
    var gameBoxMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ map: myLoadTexture( 'images/textures/wood.jpg' )}),
        .8, // high friction
        0 // low restitution
    );
    
    
    gameBoxMaterial.map.wrapS = gameBoxMaterial.map.wrapT = THREE.RepeatWrapping;
    gameBoxMaterial.map.repeat.set( 2, 2);
    
    // ------------------------------------------- ^
        var wall = new Physijs.BoxMesh(
		  new THREE.BoxGeometry( 
                tableSide,
                playerBallRadius,
                tableSide),
            gameBoxMaterial,
            0);
        wall.position.set(0, -playerBallRadius/2, 0);
        //console.log('Make wall with rotation=' + wallInfo.r + ' and position=' + wall.position.toArray().join(','));

		wall.castShadow = true;
		wall.receiveShadow = true;
		scene.add(wall);
}

function makePointLight(){
    jQuery.map([
        /*
        {xOffset: 0, yOffset: pointLightYOffset, zOffset: 300, color: 0xff0000, distanceOffset: 0},
        {xOffset: 200 * Math.cos(Math.PI * -1/6), yOffset: pointLightYOffset, zOffset: 300 * Math.sin(Math.PI * -1/6), color: 0x00ff00, distanceOffset: 0},
        {xOffset: 200 * Math.cos(Math.PI * -5/6), yOffset: pointLightYOffset, zOffset: 300 * Math.sin(Math.PI * -1/6), color: 0x0000ff, distanceOffset: 0},*/
        //{yOffset: -wallHeight / 10, zOffset: 0, color: 0xffffff, distance: wallHeight/ 8, decay: 2, posChange: false},
        //{yOffset: wallHeight/ 1.5, color: 0x99ffff, distance: wallHeight / 2, decay: 1, intensity: 0.5},
        //{yOffset: wallHeight / 4, color: 0xffff77, distance: wallHeight, decay: 0},
        {yOffset: 900
         , color: 0xffffff, distance: 0, decay: 0, intensity: 1},
        //{yOffset: wallHeight / 16, color: 0xffffff, distance: wallHeight / 4 , decay: 0, intensity: .3},
        //{yOffset: -wallHeight / 5, color: 0xffffff, distance: wallHeight , decay: 0, intensity: .5}
  ], function (lightInfo) {
        var light = new THREE.PointLight(
            lightInfo.color, 
            lightInfo.intensity || 1, // Intensity
            lightInfo.distance, // Distance
            lightInfo.decay); // decay
         light.castShadow = true;
        light.shadowCameraNear = 1;
        light.shadowCameraFar = wallHeight * 4;
        light.shadowDarkness = 0.5;
        light.shadowMapWidth = 2048;
        light.shadowMapHeight = 2048;

      var setLightPosition = function () {
          light.position.set(0, 
                             lightInfo.yOffset, 
                             0);
      };
      setLightPosition();
      scene.addEventListener('update', setLightPosition);
      scene.add(light);
  });
     }

/*
funciton makeTextMesh(){
    var i = 1;
}

function makeText3D(){
    var textMaterial = new THREE.MeshPhongMaterial({
        color: 0xdddddd
    });
    var textGeom = new THREE.TextGeometry( 'Hello World!', {
        font: 'harabara' // Must be lowercase!
        , size: 10
        , height: 10
    });
    var textMesh = new THREE.Mesh( textGeom, textMaterial );

    scene.add( textMesh );
}
*/

function makeSkyBox(){
    var cubeMap = new THREE.CubeTexture( [] );
    cubeMap.format = THREE.RGBFormat;

    myLoadImage( 'images/textures/sky/skyboxsun25degtest.png', function ( image ) {
        if (!image)
            throw new Error('Unable to load something.');
        console.log(image);

        var getSide = function ( x, y ) {

            var size = 1024;

            var canvas = document.createElement( 'canvas' );
            canvas.width = size;
            canvas.height = size;

            var context = canvas.getContext( '2d' );
            context.drawImage( image, - x * size, - y * size );

            return canvas;

        };
        cubeMap.images[ 0 ] = getSide( 2, 1 ); // px
        cubeMap.images[ 1 ] = getSide( 0, 1 ); // nx
        cubeMap.images[ 2 ] = getSide( 1, 0 ); // py
        cubeMap.images[ 3 ] = getSide( 1, 2 ); // ny
        cubeMap.images[ 4 ] = getSide( 1, 1 ); // pz
        cubeMap.images[ 5 ] = getSide( 3, 1 ); // nz
        cubeMap.needsUpdate = true;
    } );

    var cubeShader = THREE.ShaderLib[ 'cube' ];
    cubeShader.uniforms[ 'tCube' ].value = cubeMap;

    var skyBoxMaterial = new THREE.ShaderMaterial( {
        fragmentShader: cubeShader.fragmentShader,
        vertexShader: cubeShader.vertexShader,
        uniforms: cubeShader.uniforms,
        depthWrite: false,
        side: THREE.BackSide
    } );

    var skyBox = new THREE.Mesh(
        new THREE.BoxGeometry( tableWidth * 4, tableWidth * 4, tableWidth * 4),
        skyBoxMaterial
    );
    

    scene.add( skyBox );
}

var light;

function createScene()
{
    var snake;

    setSceneBasics();

    //scene.add(new THREE.AmbientLight( 0x404040 )); // soft white light
    
    setGravity();
   
    makePlayerBox();
    
    //makePlayerLayer();

    //makeText3D();

    //scene.add( new THREE.AmbientLight( 0x404040 ) );


    makeSkyBox();

    makeTable(); 

    makePointLight();


    // MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
    renderer.shadowMap.enabled = true;
    //renderer.shadowMapType = THREE.PCFSoftShadowMap; // snooth shadows

    /* Call the playerBallMovement() once every physics (simulation) frame. */
    
    snake = new Snake(new THREE.Vector2(-1, 0), Key.arrowUp, Key.arrowDown, Key.arrowLeft, Key.arrowRight);

    scene.addEventListener('update', animate);
    scene.addEventListener('update', function () {snake.animate();});
}

function draw()
{
	// Physijs simulate
    if(!Key.isDown(Key.F)) {
	   scene.simulate(); // run physics
    }
	// draw THREE.JS scene

    var i;
    for (i = 0; i < avalanche.length; i++)
        avalanche[i].onrender();

	renderer.render(scene, camera);
    
    // loop draw function call
	requestAnimationFrame(draw);
}

/*
function makeMirrorSphere(){
    // MIRROR
    // Sphere
    var sphereGeom =  new THREE.SphereGeometry( Math.random() * tableWidth/40 + tableWidth/20, 20, 20 ); // radius, segmentsWidth, segmentsHeight
	var mirrorSphereCamera = new THREE.CubeCamera( 0.1, 5000, 100 );
	// mirrorCubeCamera.renderTarget.minFilter = THREE.LinearMipMapLinearFilter;
	var mirrorSphereMaterial = new THREE.MeshBasicMaterial( { envMap: mirrorSphereCamera.renderTarget } );
	var mirrorSphere = new Physijs.SphereMesh( sphereGeom, mirrorSphereMaterial );
    
	mirrorSphere.position.set(Math.random() * (tableWidth/2), 300, Math.random() * (tableWidth/2));
    mirrorSphere.add(mirrorSphereCamera);
	//mirrorSphereCamera.position = mirrorSphere.position;
    mirrorSphere.userData = { goodguy: true };
	scene.add(mirrorSphere);

    avalanche.push({
        object: mirrorSphere,
        onrender: function () {
            mirrorSphere.visible = false;
            mirrorSphereCamera.updateCubeMap(renderer, scene);
            mirrorSphere.visible = true;
        },
        remove: function () {
            scene.remove(mirrorSphere);
            mirrorSphere.remove(mirrorSphereCamera);
        }
    });
}*/
// Materials
var avalancheMaterial = Physijs.createMaterial(
    new THREE.MeshLambertMaterial({ color: '#adf123'}),
    10, // high friction
    .2 // low restitution
);

function createAvalanche(){
    
    var chanceToMake = Math.random() * chancePerFrame;
    if (chanceToMake > 2)
        return;
    
	
    var obj;
    var boxsize = 5 + Math.random() * playerBallRadius;
    
        obj = new Physijs.BoxMesh( 
           new THREE.BoxGeometry( 
              boxsize, boxsize, boxsize),
            avalancheMaterial,
            1); // Gravity

        obj.receiveShadow = true;
        obj.castShadow = true;
    
        obj.position.set(Math.random() * tableWidth - tableWidth/2, playerBallRadius, - tableWidth);
        obj.userData = { badguy: true };
        scene.add(obj);
        obj.setLinearFactor(new THREE.Vector3(0, 0, 0));
        obj.setAngularFactor(new THREE.Vector3(0, 0, 0));
        obj.setLinearVelocity(new THREE.Vector3(0, 0, playerBallRadius*25 + score/100));
        obj.setAngularVelocity(new THREE.Vector3(0, 0, 0));
	
            // // add the sphere to the scene
    /*
    avalanche.push({
        object: obj,
        onrender: function () {},
        remove: function () {
            scene.remove(obj);
        }
    });
    */
    // Slow down the ball
	obj.setDamping( 0, 1);
    
    obj.setAngularFactor(new THREE.Vector3(2,2,2));
    
    obj.rotation.set(new THREE.Vector3(0, 25, 90));
}

function animateAvalanche() {
    jQuery.each(avalanche, function () {
        this.object.rotation.set(0, 0, 0);
        this.object.__dirtyRotation = true;
    })
}

function deleteAvalanche(z) {
    var i;
    for (i = avalanche.length - 1; i >= 0; i--){
        var entry = avalanche[i];
        if (entry.object.position.z > z) {
            avalanche.splice(i, 1);
            entry.remove();
        }
    }
}

function keepPlayerInBox(){
    // If player is too far out then put it back in
    var checkInt = playerBall.position.x + playerBall.width;
    if (checkInt > tableWidth/2){
        playerBall.__dirtyPosition = true;
        playerBall.position.x = checkInt;
    }
    if (-checkInt < tableWidth/2){
        playerBall.__dirtyPosition = true;
        playerBall.position.x = -checkInt;
    }
    checkInt = playerBall.position.z + playerBall.width;
    if (checkInt > tableWidth/2){
        playerBall.__dirtyPosition = true;
        playerBall.position.z = checkInt;
    }
    if (-checkInt < tableWidth/2){
        playerBall.__dirtyPosition = true;
        playerBall.position.z = -checkInt;
    }
}

var nextScore = 1;
var animationsPerScore = 30;

// Handles player's movement
function animate()
{
    // Keep player ball in the same place
    // --------------------- Player movement ------
    
    var impulse = new THREE.Vector3(0, 0, 0);
	if (Key.isDown(Key.D) || Key.isDown(Key.arrowRight)) {
		impulse.set(10 + score/60, 0, 0);
    }
	if (Key.isDown(Key.A) || Key.isDown(Key.arrowLeft)) {
		impulse.set(-10 - score/60, 0, 0);
	}
    
    playerBall.applyCentralImpulse(impulse);
    
}

// Snake --------------------------------------------------------------------

function Snake(direction, up, down, left, right)
{
    this.up = up;
    this.down = down;
    this.left = left;
    this.right = right;
    this.headSegment = new SnakeSegment(
        new THREE.Vector2(0, 0),
        direction);
    this.tailSegment = this.headSegment;
    this.turnDirection = direction.clone();
}

jQuery.extend(
    Snake.prototype,
    {
        animate: function() {
            this.headSegment.extend();
            if (/*Key.isDown(Key[' '])*/ true) {
                if (this.tailSegment.shrink()) {
                    this.tailSegment.destroy();
                    this.tailSegment = this.tailSegment.next;
                }
            }
            this.getTurn();
           
            var headSegmentLengthModulus = this.headSegment.length % 1;
            if (headSegmentLengthModulus < this.lastHeadSegmentLengthModulus)
               this.turn();
            this.lastHeadSegmentLengthModulus = headSegmentLengthModulus;
        },
        getTurn: function () {
            if (this.headSegment.direction.x) {
                if (Key.isDown(this.up)){
                    this.turnDirection = new THREE.Vector2(0,-1);
                }
                if (Key.isDown(this.down)) {
                    this.turnDirection = new THREE.Vector2(0,1);
                }
            } else {
                if (Key.isDown(this.left)) {
                    this.turnDirection = new THREE.Vector2(-1,0);
                }
                if (Key.isDown(this.right)) {
                    this.turnDirection = new THREE.Vector2(1,0);
                }
            }
        },
        turn: function () {
            if (!this.headSegment.direction.equals(this.turnDirection)) {
                this.headSegment = this.headSegment.turn(this.turnDirection);
            }
        }
    });



var snakeMaterial = Physijs.createMaterial(
    new THREE.MeshLambertMaterial({ color: '#599545'}),
    10, // high friction
    .2 // low restitution
);

function SnakeSegment(position, direction, previous) {
    this.startPosition = position;
    this.direction = direction;
    this.length = 1;
    this.advancement = 0;
    this.unitLength = 40;
    this.box = new Physijs.BoxMesh(
        new THREE.BoxGeometry( 
              this.unitLength, this.unitLength * 5, this.unitLength),
            snakeMaterial,
            0); // Gravity;
    
    this.box.receiveShadow = true;
    this.box.castShadow = true;
    this.box.position.set(position.x, 50, position.y);
    //this.box.userData = { badguy: false };
    scene.add(this.box);
    this.previous = previous;
    if (this.previous) {
        this.previous.next = this;
    }
}

jQuery.extend(
    SnakeSegment.prototype,
    {
        destroy: function () {
            scene.remove(this.box);
            if (this.next) {
                this.next.previous = undefined;
            }
        },
        getHeadPosition: function () {
            return this.startPosition.clone().add(this.direction.clone().multiplyScalar(this.unitLength * (this.length-1)));
        },
        dextendIncrement: 0.1,
        extend: function () {
            this.length += this.dextendIncrement;
            this.updateBox();
        },
        shrink: function () {
            /*
             * We have to use advancement when we are the head.
             * But if we use advancement when we are not the head,
             * the calculations which assume we are the head fail
             * and make the thing move when it shouldn't. Thus we
             * cheat by switching the startPosition around and
             * decreasing length.
             */
            if (this.next) {
                this.length -= this.dextendIncrement;
            } else {
                this.advancement += this.dextendIncrement;
            }
            this.updateBox();
            return (this.length - this.advancement) <=  1 && this.next;
        },
        roundLength: function () {
            this.length = Math.round(this.length);
            this.updateBox();
        },
        updateBox: function () {
            var position, scale;
            position = this.startPosition.clone().add(this.direction.clone().multiplyScalar(this.unitLength*((this.length-1 + this.advancement)/2)));
            scale = this.direction.clone().multiplyScalar(this.length - this.advancement);
            this.box.position.set(position.x, this.box.position.y, position.y);
            this.box.__dirtyPosition = true;
            this.box.scale.set(Math.abs(scale.x || 1), this.box.scale.y, Math.abs(scale.y || 1));
        },
        turn: function (direction) {
            this.roundLength();
            this.next = new SnakeSegment(
                this.getHeadPosition(),
                direction,
                this);
            
            /* Because we called roundLength() to round down the length, need to make up or that by pre-extending the new segment. */
            this.next.extend();
            
            /* Transpose around the startPosition so that something can happen. */
            this.startPosition = this.getHeadPosition();
            this.direction.multiplyScalar(-1);
            this.length -= this.advancement;
            this.advancement = 0;
            
            /* Return the new segment to caller. */
            return this.next;
        }
    });

