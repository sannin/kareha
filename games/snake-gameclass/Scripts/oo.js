'use strict';

function buildClass(
    superClass,
    build) {
        var superPrototype = (superClass || {}).prototype || null;
        var ingredients = build(superPrototype);
        if (!ingredients.constructor)
            throw new Error('You must return an object with a constructor property.');
        ingredients.constructor.prototype = Object.create(superPrototype);
        jQuery.extend(ingredients.constructor.prototype, ingredients.members);
        return ingredients.constructor;
};

var Super = buildClass(
    undefined,
    function (superPrototype) {
       return {
           constructor: function () {},
           members: {
               squawk: function () {
                   console.log('hi');
               }
           }
       } 
    });
var Sub = buildClass(
    Super,
    function (superPrototype) {
        return {
            constructor: function () {},
            members: {
                squawk: function () {
                    Super.prototype.squawk.call(this);
                    superPrototype.squawk.call(this);
                    console.log('there');
                }
            }
        }
    });
var x = new Super();
x.squawk();
x = new Sub();
x.squawk();

var o 